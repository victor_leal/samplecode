# Queries utilizadas no exercício de SQL

# 1.Query que retorna a quantidade de funcionários separados por sexo.
SELECT gender, COUNT(emp_no) FROM employees GROUP BY gender;

# 2.Query que retorna a quantidade de funcárionários distintos por sexo, ano e ano de nascimento. 
SELECT gender, YEAR(hire_date), YEAR(birth_date), COUNT(emp_no) AS Total FROM employees GROUP BY gender, YEAR(hire_date), YEAR(birth_date);

# 3.Query que retorna a média, min e max de salário por sexo.
SELECT gender, AVG(salary), MIN(salary), MAX(salary) 
FROM employees e
INNER JOIN salaries s ON e.emp_no = s.emp_no
GROUP BY gender;

# Nesse terceiro exercício, temos outras opções que podem ser consideradas:

# 1. Criar um índice na tabela salaries, na coluna salary a fim de otimizar o uso das funções MAX, MIN e AVG
# 2. Utilizar subconsultas. Poderíamos executar uma query que encontra os valores solicitados para o sexo M, outra para o sexo F e juntar os dados, por exemplo.

# Tentei criar o índice na tabela salaries mas o MySQL matou a conexão no meio do processo
# A query do exercício 3 é claramente muito lenta. Seria o caso de estudar como melhorar a performance dela, mas como entendo que já demorei mais que o esperado
# na solução dos exercícios, envio a query que trará os resultados solicitados e deixo essa observação aqui.
