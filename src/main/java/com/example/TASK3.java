package com.example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {

    private final int MIN = 0;
    private final int MAX = 1000;
    private final int TOTAL_OF_ELEMENTS = 2000;

    private List<String> lista;

    /**
     * Construtor da classe
     */
    public TASK3() {
        // Inicializamos a lista
        this.lista = new ArrayList<String>();

        // Vamos preencher a lista com uma sequência de strings, onde cada string é composta por um número aleatório de
        // um determinado range.
        for (int i = 0; i < TOTAL_OF_ELEMENTS; i++) {
            int random = MAX + (int)(Math.random() * ((MAX - MIN) + 1));
            String temp = "STRING " + random;

            this.lista.add(temp);
        }
    }

    /**
     * Função que contabiliza os diferentes elementos de uma determinada lista.
     * Para tanto, fazemos uso de uma estrutura de dados auxiliar - HashSet - que permite que um determinado elemento
     * exista apenas uma vez dentro do conjunto.
     *
     * @return int total de elementos únicos
     */
    public int countDistinctItems() {
        Set<String> aux = new HashSet<String>();

        for (String item: this.lista) {
            aux.add(item);
        }

        return aux.size();
    }

    public static void main(String args[]) {
        TASK3 t = new TASK3();

        System.out.println("Total de elementos distintos: " + t.countDistinctItems());
    }

}
