package com.example;

import java.util.LinkedList;
import java.util.List;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * Fiquei com dúvida se o exercício cobra a implementação de uma lista ligada, ou se cobra que saibamos qual estutura
 * de dados em Java nos permite fazer tal operação. Optei pelo segundo, embora, se for o caso, eu possa fazer o primeiro.
 *
 * 
 */
public class TASK2 {

    private List<Integer> lista;

    public TASK2() {
        this.lista = new LinkedList<Integer>();

        for (int i = 1; i <= 20; i++) {
            this.lista.add(i);
        }
    }

    public void printList() {
        int size = this.lista.size();

        for (int i = 0; i < size; i++ ) {
            System.out.print(this.lista.get(i));
            System.out.print(" ");
        }
    }

    public int removeElement() {
        // Deixamos hardcoded o indice do elemento que sera removido
        return this.lista.remove(9);
    }

    public static void main(String args[]) {
        TASK2 t = new TASK2();

        t.printList();
        t.removeElement();
        System.out.println();
        t.printList();
    }

}
