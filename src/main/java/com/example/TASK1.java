package com.example;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {

    private String reverseString(String original) {
        String palindrome = "";
        int length = original.length()-1;

        for (int i = length; i >=0; i--) {
            palindrome += original.charAt(i);
        }

        return palindrome;
    }

    public boolean isPalindrome(String original) {
        String palindrome = reverseString(original);

        System.out.println(original);
        System.out.println(palindrome);

        return original.equals(palindrome);
    }

    public static void main (String args[]) {
        TASK1 t = new TASK1();

        System.out.println(t.isPalindrome("madam"));
    }

}
